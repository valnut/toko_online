<div class="container-fluid">
    <h3><i class="fas fa-edit"></i> EDIT DATA BARANG</h3>
    <?php foreach ($barang as $brg) : ?>
        <form method="post" action="<?= base_url() . 'admin/data_barang/update'; ?>" enctype="multipart/form-data">

            <div>
                <input type="hidden" name="id_barang" class="form-control" value="<?= $brg->id_barang ?>">
            </div>
            <div class="form-group col-6">
                <label>Nama Barang</label>
                <input type="text" name="nama_brg" class="form-control" value="<?= $brg->nama_brg ?>">
            </div>
            <div class="form-group col-6">
                <label>Keterangan</label>
                <input type="text" name="keterangan" class="form-control" value="<?= $brg->keterangan ?>">
            </div>
            <div class="form-group col-6">
                <label>Kategori</label>
                <input type="text" name="kategori" class="form-control" value="<?= $brg->kategori ?>">
            </div>
            <div class="form-group col-6">
                <label>Harga</label>
                <input type="text" name="harga" class="form-control" value="<?= $brg->harga ?>">
            </div>
            <div class="form-group col-6">
                <label>Stok</label>
                <input type="text" name="stok" class="form-control" value="<?= $brg->stok ?>">
            </div>
            <div class="form-group col-6">
                <label>Upload Gambar</label><br>
                <input type="file" name="gambar" class="form-control" value="<?= $brg->gambar ?>">
                <img src="<?= base_url() . '/uploads/' . $brg->gambar ?>">
            </div>
            <button type="submit" class="btn btn-primary btn-sm mb-3 mt-3">
                <i class="fa fa-paper-plane"></i> Simpan</button>
            <button type="reset" class="btn btn-warning btn-sm mb-3 mt-3 mr-1">
                <i class="fa fa-sync-alt"></i> Reset</button>
        </form>
    <?php endforeach; ?>
</div>