<div class="container-fluid">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="btn btn-sm btn-info mb-4">
                <?php $grand_total = 0;
                if ($keranjang = $this->cart->contents()) 
                {
                    foreach ($keranjang as $item) 
                    {
                        $grand_total = $grand_total + $item['subtotal'];
                    }

                echo "<h4>Total Belanja Anda: Rp. ".number_format($grand_total,0,',','.');    
                ?>
            </div>

            <h3>Input Alamat Pengiriman dan Pembayaran</h3>

            <form method="post" action="<?= base_url() ?>dashboard/proses_pesanan">

            <div class="form-group">
                <label>Nama Lengkap</label>
                <input type="text" name="nama" placeholder="Nama Lengkap Anda" class="form-control">
            </div>
            <div class="form-group">
                <label>Alamat Lengkap</label>
                <input type="text" name="alamat" placeholder="Alamat Lengkap Anda" class="form-control">
            </div>
            <div class="form-group">
                <label>No. Telepon</label>
                <input type="text" name="no_telp" placeholder="Nomor Telepon Anda" class="form-control">
            </div>
            <div class="form-group">
                <label>Jasa Pengiriman</label>
                <select name="jasa_pengiriman" class="form-control">
                    <option>JNE</option>
                    <option>J&T</option>
                    <option>TIKI</option>
                    <option>POS Indonesia</option>
                    <option>GOJEK</option>
                    <option>GRAB</option>
                </select>
            </div>
            <div class="form-group">
                <label>Pilih BANK</label>
                <select name="bank" class="form-control">
                    <option>BCA - xxxxxxxxx</option>
                    <option>BNI - xxxxxxxxx</option>
                    <option>BRI - xxxxxxxxx</option>
                    <option>MANDIRI - xxxxxxxxx</option>
                    <option>CIMB - xxxxxxxxx</option>
                </select>
            </div>

            <button type="submit" class="btn btn-sm btn-success"><i class="fas fa-paper-plane"></i> Pesan</button>
            </form>

            <?php 
            }else{
                echo "<h5>Keranjang Belanja Anda Masih Kosong";
            }?>

        </div>
        <div class="col-md-2"></div>
    </div>
</div>